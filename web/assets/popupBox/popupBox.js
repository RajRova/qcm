function togglePopup(box, onConfirmFunction, confirm_args, onCancelFunction, cancel_args)
{
	var popupBox = document.getElementById(box);
	var popupBack = document.getElementById('popupBack');

    $("#" + box + " .popupBox_confirmButton")
    .off('click')
    .click(function(){if (typeof onConfirmFunction === "function") onConfirmFunction.apply(null, confirm_args); togglePopup(box);});
    
    $("#" + box + " .popupBox_cancelButton")
    .off('click')
    .click(function(){if (typeof onCancelFunction === "function") onCancelFunction.apply(null, cancel_args); togglePopup(box);});
    
	if(popupBox.style.display == "block"){    
    	//$('#' + box).slideToggle('slow');
        $('#' + box).animate({top: '-' + ($('#' + box).height() * 2)}, 500);
        $('#' + box).fadeOut("fast", function(){});
		//popupBox.style.display = "none";
		$('#popupBack').fadeOut("slow", function(){});
		//popupBack.style.display = "none";
	} else {
		//$('#' + box).slideToggle('slow');
        $('#' + box).offset({ top: '-' + ($('#' + box).height() * 2)});
        $('#' + box).animate({top: '15%'}, 1000);
		popupBox.style.display = "block";
		$('#popupBack').fadeIn("slow", function(){});
		//$('#popupBack').animate({opacity: 1}, 2000, function() {});
		popupBack.style.display = "block";
    }
}

function myConfirmPopup(title, message, onConfirmFunction, confirm_args, onCancelFunction, cancel_args, boxId)
{
    boxId = typeof boxId !== 'undefined' ? boxId : 'popbox1';
    $('#' + boxId + " .boxheader").text(title);
    $('#' + boxId + " .boxbody").text(message);
    togglePopup(boxId, onConfirmFunction, confirm_args, onCancelFunction, cancel_args);
}

function myAlertPopup(title, message, bordercolor, boxId)
{
    boxId = typeof boxId !== 'undefined' ? boxId : 'popbox2';
    if ( typeof bordercolor !== 'undefined' )
    {
        $('#' + boxId).css('background-color', bordercolor);
    }
    else
    {
        $('#' + boxId).css('background-color', null);
    }
    $('#' + boxId + " .boxheader").text(title);
    $('#' + boxId + " .boxbody").text(message);
    togglePopup(boxId);
}

function myPopup(title, message, onConfirmFunction, confirm_args, onCancelFunction, cancel_args, boxId)
{
    boxId = typeof boxId !== 'undefined' ? boxId : 'popbox1';
    $('#' + boxId + " .boxheader").text(title);
    if (typeof message !== 'undefined') $('#' + boxId + " .boxbody .text").text(message);
    togglePopup(boxId, onConfirmFunction, confirm_args, onCancelFunction, cancel_args);
}
