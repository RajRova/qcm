<?php

namespace Sdz\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class BlogController extends Controller
{
    public function indexAction()
    {
        $articles = array(
            array(
                'titre' => 'Mon weekend a Phi Phi Island !',
                'id' => 1,
                'auteur' => 'winzou',
                'contenu' => 'Ce weekend était trop bien. Blabla…',
                'date' => new \Datetime()),
            array(
                'titre' => 'Repetition du National Day de Singapour',
                'id' => 2,
                'auteur' => 'winzou',
                'contenu' => 'Bientôt prêt pour le jour J. Blabla…',
                'date' => new \Datetime()),
            array(
                'titre' => 'Chiffre d\'affaire en hausse',
                'id' => 3,
                'auteur' => 'M@teo21',
                'contenu' => '+500% sur 1 an, fabuleux. Blabla…',
                'date' => new \Datetime())
        );

        return $this->render('SdzBlogBundle:Blog:index.html.twig', array(
            'articles' => $articles
        ));
    }
    
    public function mailAction()
    {
        $session = $this->get('session');
        $user_id = $session->get('user_id');
        //$session->set('user_id', 92);
        return new Response('user_id = ' . $user_id);
    }
    
    public function voirAction($id)
    {
        $article = array(
            'id' => 1,
            'titre' => 'Mon weekend a Phi Phi Island !',
            'auteur' => 'winzou',
            'contenu' => 'Ce weekend était trop bien. Blabla…',
            'date' => new \Datetime()
        );

        return $this->render('SdzBlogBundle:Blog:voir.html.twig', array(
            'article' => $article
        ));
    }   
     
    public function ajouterAction($id)
    {
        return new Response('Ajouter');
    }
        
    public function modifierAction($id)
    {
        $article = array(
            'id' => 1,
            'titre' => 'Mon weekend a Phi Phi Island !',
            'auteur' => 'winzou',
            'contenu' => 'Ce weekend était trop bien. Blabla…',
            'date' => new \Datetime()
        );

        return $this->render('SdzBlogBundle:Blog:modifier.html.twig',
            array(
                'article' => $article
        ));
    }   
        
    public function supprimerAction($id)
    {
        return new Response('Supprimer (id = ' . $id . ')');
    }   
    
    public function menuAction($nombre)
    {
        $liste = array(
            array('id' => 2, 'titre' => 'Mon dernier weekend !'),
            array('id' => 5, 'titre' => 'Sortie de Symfony2.1'),
            array('id' => 9, 'titre' => 'Petit test')
        );
        
        return $this->render('SdzBlogBundle:Blog:menu.html.twig', array(
            'liste_articles' => $liste,
        ));
    }
}
