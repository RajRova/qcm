<?php

namespace Rova\QCMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Rova\QCMBundle\Entity\User;
use Rova\QCMBundle\Entity\Serie;
use Rova\QCMBundle\Entity\Question;
use Rova\QCMBundle\Entity\Choice;
use Rova\QCMBundle\Entity\Category;
use Rova\QCMBundle\Entity\Guest;
use Rova\QCMBundle\Entity\GuestAnswer;
use Rova\QCMBundle\Form\serieType;


class GuestController extends Controller
{
    private $navbar_username;
    
    private function getGuest()
    {
        $session = $this->get('session');
        $request = $this->getRequest();
        $guestName = $request->request->get('guest_name');
        
        $guestId = $session->get('guest_id');
        
        $em = $this->getDoctrine()->getManager();

        if ( $guestId == false )
        {
            $guest = new Guest();
            $guest->setDateCreated(new \DateTime());
            if ( $guestName ) 
            {
                $guest->setNickname( $guestName );
                $this->navbar_username = $guestName;
            }
            $em->persist( $guest );
            $em->flush();
            $session->set('guest_id', $guest->getId());
        } 
        else 
        {
            $guest = $em->getRepository('RovaQCMBundle:Guest')->find( $guestId );    
            $this->navbar_username = $guest->getNickname();
        }
        
        return $guest;
    }

    
    public function indexAction()
    {
        //$dtz = new \DateTimeZone('Etc/GMT-3');
        //$d = new \DateTime('now', $dtz);
        //echo $dtz->getOffset($d) / 3600 . '<br/>';
        //echo $d->format('d-m-Y H:i:s') . '<br/>';
        //$d->setTimezone(new \DateTimeZone('Etc/GMT-2'));
        //echo $d->format('d-m-Y H:i:s') . '<br/>';
        $guest = $this->getGuest();
        
        $data = array(
            'nickname' => $guest->getNickname(),
            'navbar_username' => $this->navbar_username,
        );
        
        return $this->render('RovaQCMBundle:Guest:home.html.twig', $data);
    }
    
    public function viewSerieListAction()
    {
        $guest = $this->getGuest();
        
        $em = $this->getDoctrine()->getManager();
        $questionSeries = $em->getRepository('RovaQCMBundle:Serie')->findAllSeriesWhereChoiceExists();
        //$questionSeries = $user->getQuestionSeries();
        
        $rows = array();
        foreach( $questionSeries as $serie )
        {
            $rows[] = array(
                'serie' => $serie,
                'nb_ans' => $em->getRepository('RovaQCMBundle:Serie')->getUsersAnswersNumberForSerie( $serie ),
                'nb_guest_ans' => $em->getRepository('RovaQCMBundle:Serie')->getGuestsAnswersNumberForSerie( $serie ),
            );
        }
        
        $data = array(
            'name' => $guest->getNickname(),
            'rows' => $rows,
            'navbar_username' => $this->navbar_username,
        );
        
        $r = $this->render('RovaQCMBundle:Guest:QSerie/view_question_serie_list.html.twig', $data);  

        return $r;
    }
    
    public function viewSerieDescAction($id)
    {
        $r = null;
        
        $em = $this->getDoctrine()->getManager();
        $qserie = $em->getRepository('RovaQCMBundle:Serie')->find($id);
        
        $data = array(
            'navbar_username' => $this->navbar_username,
            'serie' => $qserie,   
            'nb_ans' => $em->getRepository('RovaQCMBundle:Serie')->getUsersAnswersNumberForSerie( $qserie )
                      + $em->getRepository('RovaQCMBundle:Serie')->getGuestsAnswersNumberForSerie( $qserie ),         
        );
        
        $r = $this->render('RovaQCMBundle:Guest:QSerie/view_question_serie_description.html.twig', $data);
    
        return $r;
    }
    
    public function viewTotalScoresInSerieAction($id)
    {
        $guest = $this->getGuest();
        
        $r = null;

        $em = $this->getDoctrine()->getManager();
        $serie = $em->getRepository('RovaQCMBundle:Serie')->find($id);
        
        $users = $em->getRepository('RovaQCMBundle:Answer')->getUsersAnsweredInSerie( $serie );
        $guests = $em->getRepository('RovaQCMBundle:GuestAnswer')->getGuestsAnsweredInSerie( $serie );
        //scores are updated in the function getUsersAnsweredInSerie()
        
        $data = array(
            'navbar_username' => $this->navbar_username,
            'serie' => $serie,     
            'users' => $users,   
            'guests' => $guests, 
            'guest' => $guest, 
            'max_possible_score' => $em->getRepository('RovaQCMBundle:Serie')->getMaxScorePossibleInSerie( $serie ),
        );
        
        $r = $this->render('RovaQCMBundle:Guest:QSerie/view_total_scores_in_serie.html.twig', $data);

        return $r;
    }
    

    
    public function answerSerieAction($id)
    {
        $r = null;
        
        $guest = $this->getGuest();
        //$data = array();

        $em = $this->getDoctrine()->getManager();
        $qserie = $em->getRepository('RovaQCMBundle:Serie')->find($id);
            
        $data = array(
            'navbar_username' => $this->navbar_username,
            'serie' => $qserie,
        );
       
        $request = $this->getRequest();
      
        if ( $request->getMethod() == 'POST' )
        {//****************************************************** ATY OOOOOOOOOOOOO !!!! ************************* 
            $rdos = $request->request->get('rdo');
            $chks = $request->request->get('chk');
            if (!$rdos) $rdos = array();
            if (!$chks) $chks = array();
         
            $questions = $qserie->getQuestions();
            $uc = 0;
            $mc = 0;
            foreach( $questions as $question)
            {
                if ( $question->getMultipleChoice() ) $mc ++;
                else $uc ++;
            }

            
            if ( $uc > 0 || $mc > 0 )
            {
                if (!$rdos || (!$chks && $mc > 0))
                {
                    $this->get('session')->getFlashBag()->add('info_ans', 'Allez! Donnez vos r�ponses.');
                    $r = $this->render('RovaQCMBundle:Guest:QSerie/answer_question_serie.html.twig', $data);
                }   
                else
                {
                    // euh, au fait, $choice_id ihany no ilaina eto
                    $choice_ids = array();
                    
                    foreach( $rdos as $quest_id => $choice_id )
                    {
                        $choice_ids[] = $choice_id;
                    }
                    
                    foreach( $chks as $quest_id => $choices )
                    {
                        foreach( $choices as $choice_id => $blabla )
                        {
                            $choice_ids[] = $choice_id;
                        }
                    }
                    
                    $serie = $em->getRepository('RovaQCMBundle:Serie')->getSerieByChoiceId($choice_ids[0]);
                        
                    //echo $em->getRepository('RovaQCMBundle:Answer')->isSerieAnsweredByUser($serie, $user) ? 'efa voavaliny io o!' : 'tsy mbola';
                    
                    $em->getRepository('RovaQCMBundle:GuestAnswer')->deleteAnswersInSerieByGuest($serie, $guest);
                    
                    foreach( $choice_ids as $choice_id )
                    {
                        $answer = new GuestAnswer();
                        $answer
                        ->setChoice($em->getRepository('RovaQCMBundle:Choice')->find($choice_id))
                        ->setGuest($guest);
                        $em->persist($answer);
                    }
                    
                    $em->flush();
                    
                    //$r = $this->redirect( $this->generateUrl('rovaqcm_view_all_qserie_list') );
                    
                    $data['total_score'] = $em->getRepository('RovaQCMBundle:GuestAnswer')->getTotalScoreInSerieByGuest($serie, $guest);
                    $data['max_possible_score'] = $em->getRepository('RovaQCMBundle:Serie')->getMaxScorePossibleInSerie( $serie );
                    
                    $r = $this->render('RovaQCMBundle:Guest:QSerie/view_self_score_in_serie.html.twig', $data);
                }
            }  
        }
        else
        {
            if ( is_null($r) ) $r = $this->render('RovaQCMBundle:Guest:QSerie/answer_question_serie.html.twig', $data);
        }
        
        return $r;
    }    
}
