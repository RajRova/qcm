<?php

namespace Rova\QCMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Rova\QCMBundle\Entity\User;
use Rova\QCMBundle\Entity\Serie;
use Rova\QCMBundle\Entity\Question;
use Rova\QCMBundle\Entity\Choice;
use Rova\QCMBundle\Entity\Answer;
use Rova\QCMBundle\Entity\Category;
use Rova\QCMBundle\Entity\Guest;
use Rova\QCMBundle\Form\serieType;


class QCMController extends Controller
{
    private $navbar_username;
    
    private function checkBeforeRender(&$response)
    {
        $sc = $this->get('rova_qcm.sessionchecker');
        $session = $this->get('session');

        $user = $sc->checkUserLoginAndPwd();
        
        if ( $user == false )
        {
            $response = $this->redirect( $this->generateUrl('rovaqcm_login') );
        }
        else
        {
            $this->navbar_username = $user->getFirstName();
        }   
        
        return $user;      
    }
    
    public function indexAction()
    {
        $r = null;
        
        if ( $user = $this->checkBeforeRender($r) )
        {
            //$questionSeries = $this->getDoctrine()->getManager()->getRepository('RovaQCMBundle:Serie')->findAll();
            $questionSeries = $user->getQuestionSeries();
            
            $data = array(
                'name' => $user->getFirstName(),
                'question_series' => $questionSeries,
                'navbar_username' => $this->navbar_username,
            );
            
            $r = $this->render('RovaQCMBundle:QCM:index.html.twig', $data);
        }
    
        
        return $r;
    }
    
    public function viewSelfSeriesAction()
    {
        $r = null;
        
        if ( $user = $this->checkBeforeRender($r) )
        {
            $em = $this->getDoctrine()->getManager();
            //$questionSeries = $em->getRepository('RovaQCMBundle:Serie')->findAll();
            $questionSeries = $user->getQuestionSeries();
            
            $rows = array();
            foreach( $questionSeries as $serie )
            {
                $rows[] = array(
                    'serie' => $serie,
                    'nb_ans' => $em->getRepository('RovaQCMBundle:Serie')->getUsersAnswersNumberForSerie( $serie ),
                    'nb_guest_ans' => $em->getRepository('RovaQCMBundle:Serie')->getGuestsAnswersNumberForSerie( $serie ),
                );
            }
            
            $data = array(
                'name' => $user->getFirstName(),
                'rows' => $rows,
                'navbar_username' => $this->navbar_username,
            );
            
            $r = $this->render('RovaQCMBundle:QCM:QSerie/view_self_question_serie_list.html.twig', $data);
        }
    
        
        return $r;
    }
        
    public function viewSerieListAction( $userId = NULL )
    {
        $r = null;
        
        if ( $user = $this->checkBeforeRender($r) )
        {
            $em = $this->getDoctrine()->getManager();
            $questionSeries = $em->getRepository('RovaQCMBundle:Serie')->findSerieWhereUserIsNot($user);
            //$questionSeries = $user->getQuestionSeries();
            
            $rows = array();
            foreach( $questionSeries as $serie )
            {
                $rows[] = array(
                    'serie' => $serie,
                    'nb_ans' => $em->getRepository('RovaQCMBundle:Serie')->getUsersAnswersNumberForSerie( $serie ),
                    'nb_guest_ans' => $em->getRepository('RovaQCMBundle:Serie')->getGuestsAnswersNumberForSerie( $serie ),
                );
            }
            
            $data = array(
                'name' => $user->getFirstName(),
                'rows' => $rows,
                'navbar_username' => $this->navbar_username,
            );
            
            $r = $this->render('RovaQCMBundle:QCM:QSerie/view_question_serie_list.html.twig', $data);
        }
    
        
        return $r;
    }
    
    public function addSerieAction()
    {
        $r = null;

        if ( $user = $this->checkBeforeRender($r) )
        {
            $serie = new Serie();
            $form = $this->createForm(new serieType, $serie);

            $request = $this->getRequest();
            
            if ( $request->getMethod() == 'POST' )
            {
                $form->bind($request);
                if ( $form->isValid() )
                {
                    $em = $this->getDoctrine()->getManager();
                    if ( $em->getRepository('RovaQCMBundle:Serie')->findOneBy(array('user' => $user, 'serieName' => $serie->getSerieName())) )
                    {
                        $this->get('session')->getFlashBag()->add('info', 'Ce nom de serie existe déjà');
                    }
                    else
                    {
                        $user->addQuestionSerie($serie);
                        $em->persist($serie);
                        $em->flush();
                        
                        $r = $this->redirect( $this->generateUrl('rovaqcm_view_qserie', array(
                            'navbar_username' => $this->navbar_username,
                            'id' => $serie->getId(),
                        )) );
                    }
                }
            }

            if ( is_null($r) ) $r = $this->render('RovaQCMBundle:QCM:QSerie/add_question_serie.html.twig', array('form' => $form->createView()));
        }


        
        if ( is_null($r) ) $r = new Response('Reponse nllugfglul');
        
        return $r;
    }    
        
    public function editSerieAction($id)
    {
        $r = null;

        if ( $user = $this->checkBeforeRender($r) )
        {
            $em = $this->getDoctrine()->getManager();
            if ( $serie = $em->getRepository('RovaQCMBundle:Serie')->find($id) );
            {
                $oldName = $serie->getSerieName();
                $form = $this->createForm(new serieType, $serie);     
                   
                /*$formBuilder
                    ->add('serieName', 'text', array('required' => true))
                    ->add('categories', 'entity', array(
                        'class'     => 'RovaQCMBundle:Category',
                        'property'  => 'name',
                        'multiple'  => true,
                        'required'  => false)
                    );
                
                $form = $formBuilder->getForm();*/
    
                $request = $this->getRequest();
                
                if ( $request->getMethod() == 'POST' )
                {
                    //var_dump($_POST['rova_qcmbundle_serie']['categories']);
                    $form->bind($request);
                    if ( $form->isValid() )
                    {
                        $newSerie = $em->getRepository('RovaQCMBundle:Serie')->findOneBy(array('user' => $user, 'serieName' => $serie->getSerieName()));
                        if ( !$newSerie || ($newSerie!==false && $newSerie->getSerieName() == $oldName) )
                        {
                            $user->addQuestionSerie($serie);
                            $cats = $serie->getCategories();
                            $catNames = array();
                            foreach( $cats as $cat )
                            {
                                $cat0 = $em->getRepository('RovaQCMBundle:Category')->findOneBy( array( 'name' => $cat->getName() ) );
                                if ( $cat0 )
                                {
                                    $serie->removeCategory($cat);
                                    $serie->addCategory($cat0);
                                }
                            }

                            $em->persist($serie);
                            $em->flush();
                            
                            $r = $this->redirect( $this->generateUrl('rovaqcm_view_qserie', array('id' => $serie->getId())) );
                        }
                        else
                        {
                            $this->get('session')->getFlashBag()->add('info', 'Ce nom de serie existe déjà');
                        }
                    }
                }
    
                if ( is_null($r) ) $r = $this->render('RovaQCMBundle:QCM:QSerie/edit_question_serie.html.twig', array(
                    'navbar_username' => $this->navbar_username,
                    'form' => $form->createView(),
                    'serie_id' => $id
                ));
            }
        }
        
        if ( is_null($r) ) $r = new Response('Reponse nllugfglul');
        
        return $r;
    } 
    
    public function viewSerieAction($id)
    {
        //$r = null;

        if ( $this->checkBeforeRender($r) )
        {
            $em = $this->getDoctrine()->getManager();
            $qserie = $em->getRepository('RovaQCMBundle:Serie')->find($id);
            
            $data = array(
                'navbar_username' => $this->navbar_username,
                'serie' => $qserie,
                'nb_ans' => $em->getRepository('RovaQCMBundle:Serie')->getUsersAnswersNumberForSerie( $qserie ),
            );
            
            if ( is_null($r) ) $r = $this->render('RovaQCMBundle:QCM:QSerie/view_question_serie.html.twig', $data);
        }
        
        if ( is_null($r) ) $r = new Response('Reponse nllugfglul');
        
        return $r;
    }
    
    public function viewSerieDescAction($id)
    {
        $r = null;
        
        if ( $user = $this->checkBeforeRender($r) )
        {
            $em = $this->getDoctrine()->getManager();
            $qserie = $em->getRepository('RovaQCMBundle:Serie')->find($id);
            
            $data = array(
                'navbar_username' => $this->navbar_username,
                'serie' => $qserie,   
                'nb_ans' => $em->getRepository('RovaQCMBundle:Serie')->getUsersAnswersNumberForSerie( $qserie ),         
            );
            
            $r = $this->render('RovaQCMBundle:QCM:QSerie/view_question_serie_description.html.twig', $data);
        }
        
        return $r;
    }
    
    public function viewTotalScoresInSerieAction($id)
    {
        $r = null;
        
        if ( $user = $this->checkBeforeRender($r) )
        {
            $em = $this->getDoctrine()->getManager();
            $serie = $em->getRepository('RovaQCMBundle:Serie')->find($id);
            
            $users = $em->getRepository('RovaQCMBundle:Answer')->getUsersAnsweredInSerie( $serie );
            //scores are updated in the function getUsersAnsweredInSerie()
            
            $data = array(
                'navbar_username' => $this->navbar_username,
                'serie' => $serie,     
                'users' => $users,    
                'user' => $user, 
                'max_possible_score' => $em->getRepository('RovaQCMBundle:Serie')->getMaxScorePossibleInSerie( $serie ),
            );
            
            $r = $this->render('RovaQCMBundle:QCM:QSerie/view_total_scores_in_serie.html.twig', $data);
        }
        
        return $r;
    }
    
    public function deleteSerieAction($id)
    {
        //file_put_contents('d:/logilogi.txt', "blablablabla " . $id . "   \n", FILE_APPEND);
        
        $em = $this->getDoctrine()->getManager();
        $serie = $em->getRepository('RovaQCMBundle:Serie')->find($id);
        //$questions = $serie->getQuestions();
        //foreach( $questions as $question ) $em->remove($question);
        $em->remove($serie);
        $em->flush();
        
        $response = new Response(json_encode(array('id' => $id)));
        $response->headers->set('Content-Type', 'application/json');
                
        return $response;                        
    }
    
    public function answerSerieAction($id)
    {
        $r = null;
        //$data = array();

        if ( $user = $this->checkBeforeRender($r) )
        {
            $em = $this->getDoctrine()->getManager();
            $qserie = $em->getRepository('RovaQCMBundle:Serie')->find($id);
                
            $data = array(
                'navbar_username' => $this->navbar_username,
                'serie' => $qserie,
            );
           
            $request = $this->getRequest();
          
            if ( $request->getMethod() == 'POST' )
            {//****************************************************** ATY OOOOOOOOOOOOO !!!! ************************* 
                $rdos = $request->request->get('rdo');
                $chks = $request->request->get('chk');
                if (!$rdos) $rdos = array();
                if (!$chks) $chks = array();
             
                $questions = $qserie->getQuestions();
                $uc = 0;
                $mc = 0;
                foreach( $questions as $question)
                {
                    if ( $question->getMultipleChoice() ) $mc ++;
                    else $uc ++;
                }

                
                if ( $uc > 0 || $mc > 0 )
                {
                    if (!$rdos || (!$chks && $mc > 0))
                    {
                        $this->get('session')->getFlashBag()->add('info_ans', 'Allez! Donnez vos réponses.');
                        $r = $this->render('RovaQCMBundle:QCM:QSerie/answer_question_serie.html.twig', $data);
                    }   
                    else
                    {
                        // euh, au fait, $choice_id ihany no ilaina eto
                        $choice_ids = array();
                        
                        foreach( $rdos as $quest_id => $choice_id )
                        {
                            $choice_ids[] = $choice_id;
                        }
                        
                        foreach( $chks as $quest_id => $choices )
                        {
                            foreach( $choices as $choice_id => $blabla )
                            {
                                $choice_ids[] = $choice_id;
                            }
                        }
                        
                        $serie = $em->getRepository('RovaQCMBundle:Serie')->getSerieByChoiceId($choice_ids[0]);
                            
                        //echo $em->getRepository('RovaQCMBundle:Answer')->isSerieAnsweredByUser($serie, $user) ? 'efa voavaliny io o!' : 'tsy mbola';
                        
                        $em->getRepository('RovaQCMBundle:Answer')->deleteAnswersInSerieByUser($serie, $user);
                        
                        foreach( $choice_ids as $choice_id )
                        {
                            $answer = new Answer();
                            $answer
                            ->setChoice($em->getRepository('RovaQCMBundle:Choice')->find($choice_id))
                            ->setUser($user);
                            $em->persist($answer);
                        }
                        
                        $em->flush();
                        
                        //$r = $this->redirect( $this->generateUrl('rovaqcm_view_all_qserie_list') );
                        
                        $data['total_score'] = $em->getRepository('RovaQCMBundle:Answer')->getTotalScoreInSerieByUser($serie, $user);
                        $data['max_possible_score'] = $em->getRepository('RovaQCMBundle:Serie')->getMaxScorePossibleInSerie( $serie );
                        
                        $r = $this->render('RovaQCMBundle:QCM:QSerie/view_self_score_in_serie.html.twig', $data);
                    }
                }  
            }
            else
            {
                if ( is_null($r) ) $r = $this->render('RovaQCMBundle:QCM:QSerie/answer_question_serie.html.twig', $data);
            }
        }
        
        if ( is_null($r) ) $r = new Response('Reponse nllugfglul');
        
        return $r;
    }    
    
    public function addQuestionAction($serieId)
    {
        $r = null;

        if ( $user = $this->checkBeforeRender($r) )
        {
            $label = '';
            $text = '';
            $multipleChoice = false;
            
            $request = $this->getRequest();
            
            if ( $request->getMethod() == 'POST' )
            {
                $label =            $request->request->get('label');
                $text =             $request->request->get('text');
                $choicesArray =     $request->request->get('choices');
                $multipleChoice =   $request->request->get('is_multiple_choice') == false ? false : true;
                                
                if ( $label && $text )
                {
                    if ( $label != '' )
                    {
                        $question = new Question();
                        
                        foreach( $choicesArray as $choiceRow )
                        {
                            $choice = new Choice();
                            
                            $choice
                            ->setChoiceText($choiceRow['choice'])
                            ->setScore($choiceRow['score']);
                            
                            $question->addChoice($choice); 
                        }                        
                        
                        $question
                        ->setQuestionLabel($label)
                        ->setText($text)
                        ->setMultipleChoice($multipleChoice);
                        
                        $em = $this->getDoctrine()->getManager();
                        $serie = $em->getRepository('RovaQCMBundle:Serie')->find($serieId);
                        $serie->addQuestion($question);
                        
                        $em->persist($question);
                        $em->flush();
                        
                        return $this->redirect( $this->generateUrl('rovaqcm_view_qserie', array('id' => $serieId)) );
                    }
                }
            }
            
            $data = array(
                'navbar_username' => $this->navbar_username,
                'label' => $label,
                'text' => $text,
                'serieId' => $serieId,
                'multipleChoice' => $multipleChoice,
            );
            
            if ( is_null($r) ) $r = $this->render('RovaQCMBundle:QCM:Question/add_question.html.twig', $data);
        }
        
        return $r;
    }    
    
    public function editQuestionAction($id)
    {
        $response = null;

        if ( $user = $this->checkBeforeRender($response) )
        {
            $label = '';
            $text = '';
            $multipleChoice = false;
            
            $em = $this->getDoctrine()->getManager();
            $question = $em->getRepository('RovaQCMBundle:Question')->find($id);
            
            $label = $question->getQuestionLabel();
            $text = $question->getText();
            $choices = $question->getChoices();
            $multipleChoice = $question->getMultipleChoice();
            $serieId = $question->getSerie()->getId();
            $choicesArray = array();
            foreach ( $choices as $i => $choice )
            {
                $choicesArray[$i + 1] = array(
                    'choice' => $choice->getChoiceText(),
                    'score' => $choice->getScore(),
                );
            }
            
            $request = $this->getRequest();

            if ( $request->getMethod() == 'POST' )
            {
                $label =            $request->request->get('label');
                $text =             $request->request->get('text');
                $choicesArray =     $request->request->get('choices');
                $multipleChoice =   $request->request->get('is_multiple_choice') == false ? false : true;
                
                if ( $label && $text )
                {
                    if ( $label != '' )
                    {
                        foreach( $question->getChoices() as $choice )
                        {
                            $question->removeChoice($choice);
                            $em->remove($choice);
                        }
                        
                        foreach( $choicesArray as $choiceRow )
                        {
                            $choice = new Choice();
                            
                            $choice
                            ->setChoiceText($choiceRow['choice'])
                            ->setScore($choiceRow['score']);
                            
                            $question->addChoice($choice); 
                        }
                        
                        $question
                        ->setQuestionLabel($label)
                        ->setText($text)
                        ->setMultipleChoice($multipleChoice);
                        
                        $em->flush();
                        
                        return $this->redirect( $this->generateUrl('rovaqcm_view_qserie', array(
                            'navbar_username' => $this->navbar_username, 
                            'id' => $serieId
                        )) );
                    }
                }
            }
            
            $choicesArrayStr = array();
            foreach( $choicesArray as $i => $attrs )
            {
                foreach( $attrs as $attr => $val )
                    $choicesArrayStr["choices[$i][$attr]"] = $val; 
            }
            
            $data = array(
                'navbar_username' => $this->navbar_username,
                'label' => $label,
                'text' => $text,
                'serieId' => $serieId,
                'multipleChoice' => $multipleChoice,
                'choicesJSON' => json_encode($choicesArrayStr),
                'choiceCount' => count($choicesArray),
            );
            
            if ( is_null($response) ) $response = $this->render('RovaQCMBundle:QCM:Question/edit_question.html.twig', $data);
        }
        
        return $response;
    }
    
    public function deleteQuestionAction($id)
    {
        //file_put_contents('d:/logilogi.txt', "blablablabla " . $id . "   \n", FILE_APPEND);
        $this->getDoctrine()->getManager()->getRepository('RovaQCMBundle:Question')->deleteById($id);
        
        $response = new Response(json_encode(array('id' => $id)));
        $response->headers->set('Content-Type', 'application/json');

        return $response;                        
    }
    
    public function viewUserAction($id)
    {
        $response = null;

        if ( $user = $this->checkBeforeRender($response) )
        {
            $em = $this->getDoctrine()->getManager();
            $user2 = $em->getRepository('RovaQCMBundle:User')->find($id);
            $series = $em->getRepository('RovaQCMBundle:Serie')->findSerieWhereChoiceExistsByUser($user2);
            
            $data = array(
                'navbar_username' => $this->navbar_username,
                'user' => $user2,
                'series' => $series,
            );
            
            if ( is_null($response) ) $response = $this->render('RovaQCMBundle:QCM:view_user.html.twig', $data);
        }
        
        if ( is_null($response) ) $response = new Response('Misy erera');
        
        return $response;
    }
    
    public function viewUserListAction()
    {
        $response = null;

        if ( $user = $this->checkBeforeRender($response) )
        {
            $em = $this->getDoctrine()->getManager();
            $users = $em->getRepository('RovaQCMBundle:User')->getUsersNotThisOne( $user );
            
            $data = array(
                'navbar_username' => $this->navbar_username,
                'users' => $users,
            );
            
            $response = $this->render('RovaQCMBundle:QCM:view_user_list.html.twig', $data);
        }
        
        if ( is_null($response) ) $response = new Response('Misy erera');
        
        return $response;
    }
    
    public function viewCategoriesAction()
    {
        $response = null;

        if ( $user = $this->checkBeforeRender($response) )
        {
            $em = $this->getDoctrine()->getManager();
            $categories = $em->getRepository('RovaQCMBundle:Category')->getCategoriesWhereUserNot( $user );
            
            $data = array(
                'navbar_username' => $this->navbar_username,
                'categories' => $categories,
            );
            
            $response = $this->render('RovaQCMBundle:QCM:view_categories.html.twig', $data);
        }

        return $response;
    }    
}
