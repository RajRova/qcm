<?php

namespace Rova\QCMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Rova\QCMBundle\Entity\User;

class AdminController extends Controller
{
    public function loginAction()
    {
        $sc = $this->get('rova_qcm.sessionchecker');
        $session = $this->get('session');

        $request = $this->getRequest();
        $login = $request->request->get('user_login');
        $password = $request->request->get('user_password');
        
        if ( $login === false ) $login = NULL; else $session->set('user_login', $login);
        if ( $password === false ) $password = NULL; else $session->set('user_password', $password);
        
        $res = $sc->checkUserLoginAndPwd($login, $password);
        
        if ( $res )
        {
            echo 'refdjfi';
            return $this->redirect( $this->generateUrl('rovaqcm_home') );
        }
        else
        {
            return $this->render('RovaQCMBundle:admin:login.html.twig', array('user_login' => $login));
        }
    }
    
    public function logoutAction()
    {
        $session = $this->get('session');
        $login = $session->get('user_login');
        $session->remove('user_login');
        $session->remove('user_password');
        $session->remove('guest_id');
        return $this->render('RovaQCMBundle:admin:login.html.twig', array('user_login' => $login));
    }

    public function subscribeAction()
    {
        $user = new User();
        
        $formBuilder = $this->createFormBuilder($user);
        
        $formBuilder
            ->add('login', 'text')
            ->add('password', 'password')
            ->add('firstName', 'text', array('required' => false))
            ->add('lastName', 'text', array('required' => false));
        
        $form = $formBuilder->getForm();
        
        $request = $this->getRequest();
        
        if ( $request->getMethod() == 'POST' )
        {
            $form->bind($request);
            if ( $form->isValid() )
            {
                $em = $this->getDoctrine()->getManager();
                if ( $em->getRepository('RovaQCMBundle:User')->findOneBy(array('login' => $user->getLogin())) )
                {
                    $this->get('session')->getFlashBag()->add('login_info', 'Ce login existe déjà');
                }
                else
                {
                    $em->persist($user);
                    $em->flush();
                    
                    $session = $this->get('session');
                    $session->set('user_login', $user->getLogin());
                    $session->set('user_password', $user->getPassword());
                    
                    return $this->redirect( $this->generateUrl('rovaqcm_home') );
                }
            }
        }
        
        return $this->render('RovaQCMBundle:admin:subscribe.html.twig', array('form' => $form->createView()));
    }
}
