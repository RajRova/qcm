<?php

namespace Rova\QCMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Answer
 *
 * @ORM\Table(
 *  uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"user_id", "choice_id"})  
 *  }
 * )
 * @ORM\Entity(repositoryClass="Rova\QCMBundle\Entity\AnswerRepository")
 * @UniqueEntity(fields = {"user", "choice"})
 */

class Answer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /** 
     * @ORM\ManyToOne(targetEntity="Rova\QCMBundle\Entity\User", inversedBy="answers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;    
    
    /** 
     * @ORM\ManyToOne(targetEntity="Rova\QCMBundle\Entity\Choice", inversedBy="answers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $choice;    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \Rova\QCMBundle\Entity\User $user
     * @return Answer
     */
    public function setUser(\Rova\QCMBundle\Entity\User $user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Rova\QCMBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set choice
     *
     * @param \Rova\QCMBundle\Entity\Choice $choice
     * @return Answer
     */
    public function setChoice(\Rova\QCMBundle\Entity\Choice $choice)
    {
        $this->choice = $choice;
    
        return $this;
    }

    /**
     * Get choice
     *
     * @return \Rova\QCMBundle\Entity\Choice 
     */
    public function getChoice()
    {
        return $this->choice;
    }
}