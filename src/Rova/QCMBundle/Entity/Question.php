<?php

namespace Rova\QCMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Question
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rova\QCMBundle\Entity\QuestionRepository")
 */
class Question
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** 
     * @ORM\ManyToOne(targetEntity="Rova\QCMBundle\Entity\Serie", inversedBy="questions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $serie;
    
    /**
     * @var string
     *
     * @ORM\Column(name="questionLabel", type="string", length=255)
     */
    private $questionLabel;    
    
    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=255)
     */
    private $text;
        
    /**
     * @var boolean
     *
     * @ORM\Column(name="multipleChoice", type="boolean")
     */
    private $multipleChoice;
    
    /**
     * @ORM\OneToMany(targetEntity="Rova\QCMBundle\Entity\Choice", mappedBy="question", 
     * cascade={"persist", "remove"})
     */
    private $choices;
    
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set questionLabel
     *
     * @param string $questionLabel
     * @return Question
     */
    public function setQuestionLabel($questionLabel)
    {
        $this->questionLabel = $questionLabel;
    
        return $this;
    }

    /**
     * Get questionLabel
     *
     * @return string 
     */
    public function getQuestionLabel()
    {
        return $this->questionLabel;
    }

    /**
     * Set serie
     *
     * @param \Rova\QCMBundle\Entity\Serie $serie
     * @return Question
     */
    public function setSerie(\Rova\QCMBundle\Entity\Serie $serie)
    {
        $this->serie = $serie;
    
        return $this;
    }

    /**
     * Get serie
     *
     * @return \Rova\QCMBundle\Entity\Serie 
     */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Question
     */
    public function setText($text)
    {
        $this->text = $text;
    
        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }
    
    public function getTextEscaped()
    {
        return str_replace(array("\n", "\r", ' '), array('</br>', '', '&nbsp;'), htmlentities($this->text));
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->choices = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add choices
     *
     * @param \Rova\QCMBundle\Entity\Choice $choices
     * @return Question
     */
    public function addChoice(\Rova\QCMBundle\Entity\Choice $choice)
    {
        $this->choices[] = $choice;
        $choice->setQuestion($this);
        return $this;
    }

    /**
     * Remove choices
     *
     * @param \Rova\QCMBundle\Entity\Choice $choices
     */
    public function removeChoice(\Rova\QCMBundle\Entity\Choice $choice)
    {
        $this->choices->removeElement($choice);
    }

    /**
     * Get choices
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChoices()
    {
        return $this->choices;
    }

    /**
     * Set multipleChoice
     *
     * @param boolean $multipleChoice
     * @return Question
     */
    public function setMultipleChoice($multipleChoice)
    {
        $this->multipleChoice = $multipleChoice;
    
        return $this;
    }

    /**
     * Get multipleChoice
     *
     * @return boolean 
     */
    public function getMultipleChoice()
    {
        return $this->multipleChoice;
    }
}