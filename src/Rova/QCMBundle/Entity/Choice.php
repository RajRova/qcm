<?php

namespace Rova\QCMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Choice
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rova\QCMBundle\Entity\ChoiceRepository")
 */
class Choice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** 
     * @ORM\ManyToOne(targetEntity="Rova\QCMBundle\Entity\Question", inversedBy="choices")
     * @ORM\JoinColumn(nullable=false)
     */
    private $question;    

    /**
     * @var string
     *
     * @ORM\Column(name="choiceText", type="string", length=255)
     */
    private $choiceText;

    /**
     * @var integer
     *
     * @ORM\Column(name="score", type="integer")
     */
    private $score;
    
    
    //this lets us know those who has choosen the choice
    /**
     * @ORM\OneToMany(targetEntity="Rova\QCMBundle\Entity\Answer", mappedBy="choice", cascade={"persist", "remove"})
     */
    private $answers;
    
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set choiceText
     *
     * @param string $choiceText
     * @return Choice
     */
    public function setChoiceText($choiceText)
    {
        $this->choiceText = $choiceText;
    
        return $this;
    }

    /**
     * Get choiceText
     *
     * @return string 
     */
    public function getChoiceText()
    {
        return $this->choiceText;
    }

    /**
     * Set question
     *
     * @param \Rova\QCMBundle\Entity\Question $question
     * @return Choice
     */
    public function setQuestion(\Rova\QCMBundle\Entity\Question $question)
    {
        $this->question = $question;
    
        return $this;
    }

    /**
     * Get question
     *
     * @return \Rova\QCMBundle\Entity\Question 
     */
    public function getQuestion()
    {
        return $this->question;
    }
    

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Remove answers
     *
     * @param \Rova\QCMBundle\Entity\Answer $answers
     */
    public function removeAnswer(\Rova\QCMBundle\Entity\Answer $answers)
    {
        $this->answers->removeElement($answers);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Add answers
     *
     * @param \Rova\QCMBundle\Entity\Answer $answers
     * @return Choice
     */
    public function addAnswer(\Rova\QCMBundle\Entity\Answer $answer)
    {
        $this->answers[] = $answer;
        $answer->setChoice($this);
        return $this;
    }

    /**
     * Set score
     *
     * @param integer $score
     * @return Choice
     */
    public function setScore($score)
    {
        $this->score = $score;
    
        return $this;
    }

    /**
     * Get score
     *
     * @return integer 
     */
    public function getScore()
    {
        return $this->score;
    }
}