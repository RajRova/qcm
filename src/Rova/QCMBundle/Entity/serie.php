<?php

namespace Rova\QCMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * serie
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rova\QCMBundle\Entity\SerieRepository")
 */
class Serie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="serie_name", type="string", length=255)
     */
    private $serieName;
    
    /**
     * @ORM\ManyToMany(targetEntity="Rova\QCMBundle\Entity\Category", cascade={"persist"})
     */
    private $categories;
    
    /** 
     * @ORM\ManyToOne(targetEntity="Rova\QCMBundle\Entity\User", inversedBy="question_series")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;
    
    /**
     * @ORM\OneToMany(targetEntity="Rova\QCMBundle\Entity\Question", mappedBy="serie", 
     * cascade={"persist", "remove"})
     */
    private $questions;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set serieName
     *
     * @param string $serieName
     * @return serie
     */
    public function setSerieName($serieName)
    {
        $this->serieName = $serieName;
    
        return $this;
    }

    /**
     * Get serieName
     *
     * @return string 
     */
    public function getSerieName()
    {
        return $this->serieName;
    }

    /**
     * Set user
     *
     * @param \Rova\QCMBundle\Entity\User $user
     * @return serie
     */
    public function setUser(\Rova\QCMBundle\Entity\User $user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Rova\QCMBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    
    /**
     * Add questions
     *
     * @param \Rova\QCMBundle\Entity\Question $questions
     * @return serie
     */
    public function addQuestion(\Rova\QCMBundle\Entity\Question $question)
    {
        $this->questions[] = $question;
        $question->setSerie($this);
        return $this;
    }

    /**
     * Remove questions
     *
     * @param \Rova\QCMBundle\Entity\Serie $questions
     */
    public function removeQuestion(\Rova\QCMBundle\Entity\Serie $questions)
    {
        $this->questions->removeElement($questions);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Add category
     *
     * @param \Rova\QCMBundle\Entity\Category $categories
     * @return serie
     */
    public function addCategory(\Rova\QCMBundle\Entity\Category $category)
    {
        $this->categories[] = $category;
    
        return $this;
    }

    /**
     * Remove categories
     *
     * @param \Rova\QCMBundle\Entity\Category $categories
     */
    public function removeCategory(\Rova\QCMBundle\Entity\Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
}