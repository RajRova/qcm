<?php

namespace Rova\QCMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rova\QCMBundle\Entity\UserRepository")
 */
class User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=255, unique=true)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255, nullable=true)
     */
    private $lastName;
    
    /**
     * @ORM\OneToMany(targetEntity="Rova\QCMBundle\Entity\Serie", mappedBy="user")
     */
    private $question_series;    
    
    /**
     * @ORM\OneToMany(targetEntity="Rova\QCMBundle\Entity\Answer", mappedBy="user")
     */
    private $answers;
    
    private $score;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set login
     *
     * @param string $login
     * @return User
     */
    public function setLogin($login)
    {
        $this->login = $login;
    
        return $this;
    }

    /**
     * Get login
     *
     * @return string 
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    
        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->question_series = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add question_series
     *
     * @param \Rova\QCMBundle\Entity\Serie $questionSeries
     * @return User
     */
    public function addQuestionSerie(\Rova\QCMBundle\Entity\Serie $questionSerie)
    {
        $this->question_series[] = $questionSerie;
        $questionSerie->setUser($this);
        return $this;
    }

    /**
     * Remove question_series
     *
     * @param \Rova\QCMBundle\Entity\Serie $questionSeries
     */
    public function removeQuestionSerie(\Rova\QCMBundle\Entity\Serie $questionSerie)
    {
        $this->question_series->removeElement($questionSerie);
    }

    /**
     * Get question_series
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getQuestionSeries()
    {
        return $this->question_series;
    }
    
    public function getFullName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }
    
    public function setScore( $score )
    {
        $this->score = $score;
        return $this;
    }
    
    public function getScore()
    {
        return $this->score;
    }
}