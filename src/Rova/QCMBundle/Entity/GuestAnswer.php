<?php

namespace Rova\QCMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GuestAnswer
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rova\QCMBundle\Entity\GuestAnswerRepository")
 */
class GuestAnswer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /** 
     * @ORM\ManyToOne(targetEntity="Rova\QCMBundle\Entity\Guest", inversedBy="answers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $guest;    
    
    /** 
     * @ORM\ManyToOne(targetEntity="Rova\QCMBundle\Entity\Choice", inversedBy="answers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $choice;   




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guest
     *
     * @param \Rova\QCMBundle\Entity\Guest $guest
     * @return GuestAnswer
     */
    public function setGuest(\Rova\QCMBundle\Entity\Guest $guest)
    {
        $this->guest = $guest;
    
        return $this;
    }

    /**
     * Get guest
     *
     * @return \Rova\QCMBundle\Entity\Guest 
     */
    public function getGuest()
    {
        return $this->guest;
    }

    /**
     * Set choice
     *
     * @param \Rova\QCMBundle\Entity\Choice $choice
     * @return GuestAnswer
     */
    public function setChoice(\Rova\QCMBundle\Entity\Choice $choice)
    {
        $this->choice = $choice;
    
        return $this;
    }

    /**
     * Get choice
     *
     * @return \Rova\QCMBundle\Entity\Choice 
     */
    public function getChoice()
    {
        return $this->choice;
    }
}