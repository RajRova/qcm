<?php

namespace Rova\QCMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Guest
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rova\QCMBundle\Entity\GuestRepository")
 */
class Guest
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nickname", type="string", length=255, nullable=true)
     */
    private $nickname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="date")
     */
    private $dateCreated;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ip_adress", type="string")
     */
    private $ip_adress;    
    
    /**
     * @ORM\OneToMany(targetEntity="Rova\QCMBundle\Entity\GuestAnswer", mappedBy="guest"
     * , cascade={"persist", "remove"})
     */
    private $answers;
    
    private $score;    
    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        //$this->dateCreated = \DateTime();
        $this->ip_adress = $_SERVER['REMOTE_ADDR'];
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nickname
     *
     * @param string $nickname
     * @return Guest
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;
    
        return $this;
    }

    /**
     * Get nickname
     *
     * @return string 
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     * @return Guest
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    
        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime 
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Add answers
     *
     * @param \Rova\QCMBundle\Entity\GuestAnswer $answers
     * @return Guest
     */
    public function addAnswer(\Rova\QCMBundle\Entity\GuestAnswer $answers)
    {
        $this->answers[] = $answers;
    
        return $this;
    }

    /**
     * Remove answers
     *
     * @param \Rova\QCMBundle\Entity\GuestAnswer $answers
     */
    public function removeAnswer(\Rova\QCMBundle\Entity\GuestAnswer $answers)
    {
        $this->answers->removeElement($answers);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAnswers()
    {
        return $this->answers;
    }
    
    public function setScore( $score )
    {
        $this->score = $score;
        return $this;
    }
    
    public function getScore()
    {
        return $this->score;
    }    

    /**
     * Set ip_adress
     *
     * @param string $ipAdress
     * @return Guest
     */
    public function setIpAdress($ipAdress)
    {
        $this->ip_adress = $ipAdress;
    
        return $this;
    }

    /**
     * Get ip_adress
     *
     * @return string 
     */
    public function getIpAdress()
    {
        return $this->ip_adress;
    }
}