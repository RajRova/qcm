<?php

namespace Rova\QCMBundle\Services;

class SessionChecker
{
    protected $doctrine;
    protected $session;
    
    public function __construct($doctrine, $session)
    {
        $this->doctrine = $doctrine;
        $this->session = $session;
    }
    
    public function test()
    {
        $repository = $this->doctrine->getManager()->getRepository('RovaQCMBundle:User');
        $user1 = $repository->find(1);
        return $user1->getFirstName();
    }
    
    /* returns a user entity or "false" */
    public function checkUserLoginAndPwd($login = NULL, $password = NULL)
    {
        if ( is_null($login) )
        {
            $login = $this->session->get('user_login');
        }

        if ( is_null($password) )
        {
           $password = $this->session->get('user_password');
        }

        if ( ! is_null($login) ) if ( $login != '' )
        {    
            if ( is_null($password) )
            {
                $this->session->getFlashBag()->add('info', "Il faut ecrire le mot de passe");
            } 
            else 
            {
                $repository = $this->doctrine->getManager()->getRepository('RovaQCMBundle:User');
                $users = $repository->findBy(array('login' => $login));
            
                foreach ( $users as $user )
                {
                    if ( $user->getLogin() == $login && $user->getPassword() == $password )
                    {
                        return $user;
                    }
                    else if ( $user->getLogin() == $login && $user->getPassword() != $password )
                    {
                        $this->session->getFlashBag()->add('info', 'Wrong password');
                        return false;
                    }
                }
                                
                if ( Count($users) == 0 ) 
                    $this->session->getFlashBag()->add('info', "Ce login n'existe pas");
                else
                    $this->session->getFlashBag()->add('info', "Vérifiez bien majuscule et minuscule");
            }
        }
        
        return false;
    }
}