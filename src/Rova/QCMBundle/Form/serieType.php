<?php

namespace Rova\QCMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class serieType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('serieName', 'text', array('required' => true))
            /*->add('categories', 'entity', array(
                        'class'     => 'RovaQCMBundle:Category',
                        'property'  => 'name',
                        'multiple'  => true,
                        'required'  => false))*/
            //->add('user', 'entity', array('class'=>'RovaQCMBundle:User','property'=>'fullName' ) )
            ->add('categories', 'collection', array(
                        'type'          => new CategoryType(),
                        'allow_add'     => true,
                        'allow_delete'  => true))
            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Rova\QCMBundle\Entity\serie'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rova_qcmbundle_serie';
    }
}
